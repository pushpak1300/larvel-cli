# LARVEL CLI GAME

### To install the application

```bash
git clone https://gitlab.com/pushpak1300/larvel-cli.git
cd larvel-cli
composer install
```

### To start the game

```bash
php artisan start:game
```