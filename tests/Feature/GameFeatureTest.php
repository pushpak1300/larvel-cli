<?php

namespace Tests\Feature;

use Tests\TestCase;

class ExampleTest extends TestCase
{
    /** @test */
    public function check_team_a_is_winner()
    {
        $this->artisan('start:game')
         ->expectsQuestion('Enter A Teams players', '35, 100, 20, 50, 40')
         ->expectsQuestion('Enter B Teams players', '35, 10, 30, 20, 90')
         ->expectsOutput('WIN')
         ->assertExitCode(0);
    }
    
    /** @test */
    public function check_team_a_is_looser()
    {
        $this->artisan('start:game')
         ->expectsQuestion('Enter A Teams players', '35, 10, 20, 50, 25')
         ->expectsQuestion('Enter B Teams players', '35, 10, 30, 20, 90')
         ->expectsOutput('LOSE')
         ->assertExitCode(0);
    }

    /** @test */
    public function check_team_a_is_winnner_with_same_values()
    {
        $this->artisan('start:game')
         ->expectsQuestion('Enter A Teams players', '36, 11, 21, 51, 26')
         ->expectsQuestion('Enter B Teams players', '35, 10, 20, 50, 25')
         ->expectsOutput('WIN')
         ->assertExitCode(0);
    }
    
    /** @test */
    public function expect_question_from_game()
    {
        $this->artisan('start:game')
         ->expectsQuestion('Enter A Teams players', '35, 100, 20, 50, 40')
         ->expectsQuestion('Enter B Teams players', '35, 10, 30, 20, 90')
         ->assertExitCode(0);
    }
}
