<?php

namespace App\Services;

use App\Contracts\GameInterface;

class GameService implements GameInterface
{
    /**
     * Check If winner is Team A based on logic
     *
     * @param Collection $collectionTeam1 (Team1 array)
     * @param Collection $collectionTeam2 (Team2 array)
     *
     * @return string
     *
     */
    public function checkWinnerIsTeamA($collectionTeam1, $collectionTeam2)
    {
        //1. Sort the collection and get the array of values
        $sortedTeam1=$collectionTeam1->sort()->values()->all();
        $sortedTeam2=$collectionTeam2->sort()->values()->all();

        //2. Check if value of array A is lower than value from
        // array B and return WIN or lose based on that
        for ($index=0; $index < 5; $index++) {
            if ($sortedTeam1[$index]<=$sortedTeam2[$index]) {
                return 'LOSE';
            } else {
                continue;
            }
        }
        return 'WIN';
    }
}
