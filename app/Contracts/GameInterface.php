<?php
namespace App\Contracts;

interface GameInterface
{
    public function checkWinnerIsTeamA($team1, $team2);
}
