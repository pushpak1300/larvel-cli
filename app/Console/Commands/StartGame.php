<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\GameService;

class StartGame extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'start:game';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the game winner based on input.';

    public $game;

    /**
     * Create a new command instance.
     *
     * @param Gameservice $game 
     * @return void
     */
    public function __construct(GameService $game)
    {
        parent::__construct();
        $this->game=$game;
    }

    /**
     * Create Collection characters from string
     *
     * @param string
     * @return Collection
     */
    private function createCollection($string)
    {
        $array=explode(',', $string);
        return collect($array);
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $teamA= $this->ask('Enter A Teams players');
        $teamB = $this->ask('Enter B Teams players');
        $collectionTeam1=$this->createCollection($teamA);
        $collectionTeam2=$this->createCollection($teamB);
        $result=$this->game->checkWinnerIsTeamA($collectionTeam1, $collectionTeam2);
        $this->info($result);
    }
}
